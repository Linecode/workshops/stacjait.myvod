using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using StacjaIt.MyVod.Domain.Movies.Events;

namespace StacjaIt.MyVod.Domain.Converter.Handlers.Events
{
    public class MovieCreatedListener : INotificationHandler<MovieCreated>
    {
        public async Task Handle(MovieCreated notification, CancellationToken cancellationToken)
        {
            Console.WriteLine("Movie Created Listener started");
            await Task.Delay(2000, cancellationToken);
            Console.WriteLine("Movie Created Listener ended");
            
            notification.Movie.AddFormat();
        }
    }
}