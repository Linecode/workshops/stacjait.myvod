using System;
using System.Collections.Generic;
using StacjaIt.MyVod.SharedKernel;

namespace StacjaIt.MyVod.Domain.Movies
{
    public class Money : ValueObject<Money>
    {
        public decimal Value { get; private set; }
        public Currencies Currency { get; private set; }

        [Obsolete("Only For EFCore", true)]
        private Money()
        {
        }

        public Money(decimal value, Currencies currency)
        {
            Value = value;
            Currency = currency;
        }

        public enum Currencies
        {
            PLN,
            EUR
        }
        
        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
            yield return Currency;
        }
    }
}