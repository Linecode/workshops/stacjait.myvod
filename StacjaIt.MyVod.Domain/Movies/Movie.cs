using System;
using System.Collections.Generic;
using System.Linq;
using StacjaIt.MyVod.Domain.Movies.Events;
using StacjaIt.MyVod.SharedKernel;

namespace StacjaIt.MyVod.Domain.Movies
{
    public sealed class Movie : Aggregate<MovieId>
    {
        public string Title { get; private set; }
        public string Description { get; private set; }
        public Money Price { get; private set; }

        private readonly List<CastMember> _cast = new();
        public IReadOnlyCollection<CastMember> Cast => _cast.AsReadOnly();

        public Movie(string title, string description)
        {
            Title = title;
            Description = description;
            
            Id = MovieId.New();
            
            Raise(new MovieCreated(this));
        }

        public void AddFormat() {}


        // { type: "UpdateMetaData", "payload": "{ title: dsadadsa, description: "dsadsadas" }" }
        // { type: "DefineCastEvent", "payload" : { castMembers: [] } }
        // { type: "Snapshot", "payload": {} }
        
        // public void DefineCast(List<CastMember> castMembers)
        // {
        //     _cast.RemoveAll(x => true);
        //     _cast.AddRange(castMembers);
        // }
        //
        // public void SetTitle(string title) => Title = title;
        // public void SetDescription(string description) => Description = description;

        public void Apply(Events.DefineCastEvent @event)
        {
            _cast.RemoveAll(x => true);
            _cast.AddRange(@event.CastMembers.ToList());
        }

        public void Apply(Events.UpdateMetaData @event)
        {
            Title = @event.Title;
            Description = @event.Description;
        }

        public static class Events
        {
            public class DefineCastEvent
            {
                public ICollection<CastMember> CastMembers { get; init; }
            }

            public class UpdateMetaData
            {
                public string Title { get; set; }
                public string Description { get; set; }
            }
        }
    }
}