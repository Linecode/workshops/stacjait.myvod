using System.Threading;
using System.Threading.Tasks;
using MediatR;
using StacjaIt.MyVod.Domain.Movies.Commands;
using StacjaIt.MyVod.Domain.Movies.Repositories;

namespace StacjaIt.MyVod.Domain.Movies.Handlers
{
    public class CreateMovieHandler : IRequestHandler<CreateMovieCommand>
    {
        private readonly IMoviesRepository _repository;

        public CreateMovieHandler(IMoviesRepository repository)
        {
            _repository = repository;
        }
        
        public async Task<Unit> Handle(CreateMovieCommand request, CancellationToken cancellationToken)
        {
            /*var castMember = new CastMember("Bruce", "Willis");
            
            _repository.Add(castMember);
            await _repository.UnitOfWork.SaveChangesAsync(cancellationToken);

            var cast = await _repository.Get(castMember.Id);*/

            var movie = new Movie("Die Hard", "Yupi Keuy yayyy");
            
            _repository.Add(movie);
            await _repository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
            
            
            return Unit.Value;
        }
    }
}