﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StacjaIt.MyVod.Domain.Movies.Commands;
using StacjaIt.MyVod.Domain.Movies.Repositories;
using StacjaIt.MyVod.Infrastructure;

var serviceProvider = new ServiceCollection()
    .AddDbContext<MoviesContext>()
    .AddMediatR(typeof(CreateMovieCommand))
    // .AddTransient<ICastMemberRepository, CastMemberRepository>()
    .AddTransient<IMoviesRepository, MoviesRepository>()
    .BuildServiceProvider();

var host = Task.Run(async () =>
{
    await new HostBuilder()
        .ConfigureServices((context, services) =>
        {
            services.AddDbContext<MoviesContext>();
            services.AddMediatR(typeof(CreateMovieCommand));
            services.AddHostedService<OutboxHostedService>();
        })
        .RunConsoleAsync();
});

using var scope = serviceProvider.CreateScope();

#region DatabaseRecreation

var context = scope.ServiceProvider.GetRequiredService<MoviesContext>();
await context.Database.EnsureDeletedAsync();
await context.Database.EnsureCreatedAsync();

#endregion

var sender = scope.ServiceProvider.GetRequiredService<ISender>();
await sender.Send(new CreateMovieCommand());

await host;