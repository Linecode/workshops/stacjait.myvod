using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using StacjaIt.MyVod.Domain.Movies;

namespace StacjaIt.MyVod.Infrastructure
{
    public class OutboxHostedService : BackgroundService
    {
        private readonly IServiceProvider _services;

        public OutboxHostedService(IServiceProvider services)
        {
            _services = services;
        }
        
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(1000, stoppingToken);

                using var scope = _services.CreateScope();

                var context = scope.ServiceProvider.GetRequiredService<MoviesContext>();
                var mediator = scope.ServiceProvider.GetRequiredService<IMediator>();

                var messages = context.Outbox.ToList();

                foreach (var message in messages)
                {
                    var type = Assembly.GetAssembly(typeof(Movie)).GetType(message.Type);
                    var notification = JsonConvert.DeserializeObject(message.Data, type);
                    await mediator.Publish((INotification) notification);
                }
                
                context.Outbox.RemoveRange(messages);
                await context.SaveChangesAsync(stoppingToken);
            }
        }
    }
}