using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StacjaIt.MyVod.Domain.Movies;

namespace StacjaIt.MyVod.Infrastructure
{
    public class MovieEntityTypeConfiguration : IEntityTypeConfiguration<Movie>
    {
        private const string Timestamp = nameof(Timestamp);
        private const string UpdatedAt = nameof(UpdatedAt);
        
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id)
                .HasConversion(x => x.Value, x => new MovieId(x));

            builder.Ignore(x => x.DomainEvents);

            builder.Property(x => x.Title);
            builder.Property(x => x.Description);

            builder.Property<byte[]>(Timestamp)
                .IsRowVersion();

            builder.OwnsOne(x => x.Price, p =>
            {
                p.Property(x => x.Currency)
                    .HasConversion<string>()
                    .HasColumnName("Price_Currency");
                p.Property(x => x.Value)
                    .HasColumnName("Price_Value");

                // p.ToTable("MoviePrices", MoviesContext.Schema);
            });

            builder.HasMany(x => x.Cast);

            builder.ToTable("Movies", MoviesContext.Schema);
        }
    }
}