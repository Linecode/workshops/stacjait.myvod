using StacjaIt.MyVod.Domain.Movies;
using StacjaIt.MyVod.Domain.Movies.Repositories;
using StacjaIt.MyVod.SharedKernel;

namespace StacjaIt.MyVod.Infrastructure
{
    public class MoviesRepository : IMoviesRepository
    {
        private readonly MoviesContext _context;
        public IUnitOfWork UnitOfWork => _context;

        public MoviesRepository(MoviesContext context)
        {
            _context = context;
        }
        
        public void Add(Movie movie)
        {
            _context.Movies.Add(movie);
        }
    }
}