using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using StacjaIt.MyVod.Domain.Movies;
using StacjaIt.MyVod.SharedKernel;

namespace StacjaIt.MyVod.Infrastructure
{
    public class MoviesContext : DbContext, IUnitOfWork
    {
        private readonly IMediator _mediator;

        public MoviesContext(DbContextOptions<MoviesContext> options, IMediator mediator) : base(options)
        {
            _mediator = mediator;
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<OutboxMessage> Outbox { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        { 
            optionsBuilder.UseSqlServer(@"Server=localhost,1433;Database=MyVod;User Id=SA;Password=yourStrong(!)Password");
            optionsBuilder.LogTo(Console.WriteLine)
                .EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(MoviesContext).Assembly);
            
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            // ChangeTracker.DetectChanges();
            // var timestamp = DateTime.UtcNow;
            //
            // foreach (var entry in ChangeTracker.Entries().Where(e => e.State == EntityState.Added || e.State == EntityState.Modified))
            // {
            //     entry.Property("UpdatedAt").CurrentValue = timestamp;
            // }
            //
            return base.SaveChanges();
        }

        public static readonly string Schema = "movies";
        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
        {
            var domainEntities = ChangeTracker.Entries<IEntity>()
                .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any());

            var domainEvents = domainEntities
                .SelectMany(x => x.Entity.DomainEvents)
                .ToList();
            
            domainEntities.ToList().ForEach(entity => entity.Entity.ClearDomainEvents());

            foreach (var domainEvent in domainEvents)
            {
                var type = domainEvent.GetType().FullName;
                var data = JsonConvert.SerializeObject(domainEvent, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
                var outboxMessage = new OutboxMessage(DateTime.UtcNow, type, data);
                Outbox.Add(outboxMessage);
            }
            
            await SaveChangesAsync(cancellationToken);
            
            // var domainEntities = ChangeTracker.Entries<IEntity>()
            //     .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any());
            //
            // var domainEvents = domainEntities
            //     .SelectMany(x => x.Entity.DomainEvents)
            //     .ToList();
            //
            // domainEntities.ToList().ForEach(entity => entity.Entity.ClearDomainEvents());
            //
            // var taks = domainEvents.Select(async (domainEvents) =>
            // {
            //     await _mediator.Publish(domainEvents, cancellationToken).ConfigureAwait(false);
            // });
            //
            // await Task.WhenAll(taks).ConfigureAwait(false);
            //
            return true;
        }
    }
}