using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace StacjaIt.MyVod.Infrastructure
{
    public class OutboxMessage
    {
        public Guid Id { get; private set; }
        public DateTime OccurredOn { get; private set; }
        public string Type { get; private set; }
        public string Data { get; private set; }

        private OutboxMessage()
        {
            
        }

        internal OutboxMessage(DateTime occurredOn, string type, string data)
        {
            OccurredOn = occurredOn;
            Type = type;
            Data = data;
            
            Id = Guid.NewGuid();
        }
    }

    internal class OutboxMessageEntityTypeConfiguration : IEntityTypeConfiguration<OutboxMessage>
    {
        public void Configure(EntityTypeBuilder<OutboxMessage> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id)
                .ValueGeneratedNever();

            builder.ToTable("OutboxMessages", MoviesContext.Schema);
        }
    }
}